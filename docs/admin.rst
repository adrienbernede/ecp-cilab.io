Administration
==============

.. _introduction: introduction.html#enhancements
.. _official documentation: https://docs.gitlab.com

Documentation to support administrating GitLab runners and servers targeting
HPC environments. The information provided here is meant to supplement the
`official documentation`_ in areas where it may
not address HPC focused questions while also providing comprehensive details
on all ECP CI enchantments. For details on using the runners themselves
please see the `CI users <ci-users.html>`_ documentation.

Jacamar CI
----------

Jacamar is the HPC focused CI/CD driver for GitLab's
`custom executor <https://docs.gitlab.com/runner/executors/custom.html>`_.
The core goal of this project is to establish a maintainable, yet extensively
configurable tool that will bridge the gap between GitLab's
testing model and the requirements of a HPC computing environment. The
goal being to provide code teams with access to the well tuned facility
managed environment their project will ultimately run on during
software development within a CI/CD infrastructure.

.. toctree::
   :maxdepth: 1

   admin/jacamar/introduction.rst
   admin/jacamar/tutorial.rst
   admin/jacamar/deployment.rst
   admin/jacamar/configuration.rst
   admin/jacamar/auth.rst
   admin/jacamar/executors.rst
   admin/jacamar/troubleshoot.rst

Though the `open-source project <https://gitlab.com/ecp-ci/jacamar-ci>`_ is
still being developed, it has reached a stage where we want to encourage early
adoption. Any feedback or identifiable requirements are appreciated.

Server
------

`GitLab <https://about.gitlab.com/what-is-gitlab/>`_ offers a
comprehensive development platform all within a single application. A major
benefit to using this tool is its both predominately
`open source <https://gitlab.com/gitlab-org/gitlab>`_ and allows for
management of self-hosted instances. However, there are a
`number of features <https://about.gitlab.com/pricing/self-managed/feature-comparison/>`_
that necessitate the purchase of a license.

All documentation organized here is not written to completely replace
`official documentation`_ offered by GitLab. We only seek to highlight
specifics that are directly related to HPC centers. We fully advise using
GitLab's documentation wherever possible as it provides up-to-date information.

.. toctree::
   :maxdepth: 2

   admin/server-admin.rst

Federation
~~~~~~~~~~

At the core of the ECP enhancement efforts is the concept of federation
With this we are targeting expanding GitLab's current authentication model
to allow the management of multiple providers to expand beyond
the login process. Fundamentally a user's valid authentication
provider will become a central part of the CI job process.

Though we are currently testing these enhancements there is an
active effort to improve and ensure that they are merged into
upstream GitLab.

.. toctree::
   :maxdepth: 2

   admin/federation.rst

Supporting documentation on such functionality is an important to
us. As such we are actively working to organize more documentaiton
in conjunction with ongoing federation improvement efforts and will update
once more information is available.

Auditing
~~~~~~~~

Auditing a user's interactions with a GitLab deployment is an important
administrative component. Although the runner may be completely open-source
there are aspects of the server, including
`Audit Event <https://docs.gitlab.com/ee/administration/audit_events.html>`_,
that require a paid subscription. To understanding logging and what can
be accomplished with your given license please see the official
`log system <https://docs.gitlab.com/ee/administration/logs.html>`_
administrator documentation.

CI Token Broker
---------------

.. important::

    The `CI Token Broker <https://gitlab.com/ecp-ci/ci-token-broker>`_
    is under active development and subject to change.
    We **strongly** advise that you only deploy in a testing
    capacity until the official 1.0 release.

Manage and enforce token access during CI jobs when a service account
model is used.

.. toctree::
   :maxdepth: 2

   admin/broker/administration.rst
   admin/broker/troubleshoot.rst

Latest Releases
---------------

Detailed notes for all software releases can be found
`here <releasenotes/all.html>`_.

.. toctree::
   :maxdepth: 1

   releasenotes/jacamar/jacamar_0.5.0.rst
   releasenotes/broker/broker_0.2.0.rst
