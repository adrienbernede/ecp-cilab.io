Report Build Status to GitLab/GitHub
====================================

This example will introduce you to a workflow that can be leveraged
to update a pipeline's build status on a remote GitLab or GitHub
repository. When reading the official
`repository mirroring <https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html>`_
documentation you'll undoubtedly find there are several
limitations when interacting with specific source project
hosts:

* **GitLab**: Reporting pipeline status is not supported at all.
* **GitHub**: Reporting pipeline status is only supported via a
  Premium license.

Overcoming these limitations can be accomplished within your target
project's CI pipeline.

.. image:: files/build_status_flow.png

We will be using the
`GitLab <https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit>`_/`GitHub <https://developer.github.com/v3/repos/statuses/#create-a-commit-status>`_
API to post the build status to a specific commit.
Though it may possible to accomplish this using only the command line,
I've found easier to manage and more secure using Python.
If you choose to write your own solution please be aware
that you may be running on a multi-tenant system and to
avoid exposing any token via the command line in ``/proc``.

Lets start by creating a ``build-status.py`` script:

.. tabs::

    .. tab:: GitLab Source Project

        .. literalinclude:: ../../source/build-status-gitlab/gitlab-status.py
            :language: python
            :linenos:

        In the above script we identify all variables from the environment.
        You may recognize that we have used several
        `predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
        that are provided to each CI job as well as several of our own:

        * **BUILDSTATUS_PROJECT**: The upstream project ID.
        * **BUILDSTATUS_APIURL**: The sites API url (e.g. https://gitlab.com/api/v4)
        * **BUILDSTATUS_TOKEN**: An ``api`` scoped
          `personal access token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_.
        * **BUILDSTATUS_JOB**: Name of the job as it will appear on the source project.

    .. tab:: GitHub Source Project

        .. literalinclude:: ../../source/build-status-gitlab/github-status.py
            :language: python
            :linenos:

        In the above script we identify all variables from the environment.
        You may recognize that we have used several
        `predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
        that are provided to each CI job as well as several of our own:

        * **BUILDSTATUS_OWNER**: Project owner/group as it appears in the repository's url.
        * **BUILDSTATUS_PROJECT**: Project name as it appears in the repository's url.
        * **BUILDSTATUS_APIURL**: The sites API url (e.g. https://api.github.com)
        * **BUILDSTATUS_TOKEN**: A  ``repo:status`` scoped
          `access token <https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token>`_.
        * **BUILDSTATUS_JOB**:  Name of the job as it will appear on the source project.

We need to define several
`variables <https://docs.gitlab.com/ee/ci/variables/README.html>`_
in the target project's: **Settings** --> **CI/CD** --> **Variables**

We strongly advise that the **BUILDSTATUS_TOKEN** variable is kept in this setting
and is properly
`masked <https://docs.gitlab.com/ee/ci/variables/README.html#masked-variables>`_
to avoid exposing them publicly in the CI job log. Other variables can be
defined here or even in your CI yaml file if desired.

    .. image:: files/project-variables-status.png
        :scale: 40%

Finally once you've updated your project's CI/CD settings and added
the necessary script you should incorporate it into your pipeline
(the ``.gitlab-ci.yml`` file). The example jobs/stages below are envisioned
as a wrappers around an existing stages using the
`.pre and .post <https://docs.gitlab.com/ee/ci/yaml/#pre-and-post>`_
functionality.

.. tabs::

    .. tab:: Generic

        .. literalinclude:: ../../source/build-status-gitlab/.gitlab-ci.generic.yml
            :language: yaml
            :linenos:

    .. tab:: ALCF

        This is a portion of the workflow we are using with
        out projects to support testing and feedback from the facilities.

        .. literalinclude:: ../../source/build-status-gitlab/.gitlab-ci.alcf.yml
            :language: yaml
            :linenos:

    .. tab:: OLCF

        This is a portion of the workflow we are using with
        out projects to support testing and feedback from the facilities.

        .. literalinclude:: ../../source/build-status-gitlab/.gitlab-ci.olcf.yml
            :language: yaml
            :linenos:

After completion trigger a pipeline from the target project and then
examining the source should reveal:

.. tabs::

    .. tab:: GitLab Source Project

        .. image:: files/gitlab-pipeline-status.png
            :scale: 50%

    .. tab:: GitHub Source Project

        .. image:: files/github-pipeline-status.png
            :scale: 40%

Note that in either case you will need to ensure that the GitLab CI project
pipelines are publicly available or that necessary reviewers have access
to the instance for the link (``CI_PIPELINE_URL``) to function.
