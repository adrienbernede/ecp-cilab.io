HPC GitLab-Runner 13.0.1~hpc.0.6.1
==================================

* *Release Date*: 7/16/2020
* *Commit*: d7a038cf
* *Tag*: `v13.0.1-hpc.0.6.1 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v13.0.1-hpc.0.6.1>`_

There are no HPC focused changes (e.g. batch executor) in
this release. As such the `golang-hpc <https://gitlab.com/ecp-ci/golang-hpc>`_
version remains the same and the release RPM will be name as
*gitlab-runner-0.6.1-3.<arch>.rpm* to avoid breaking established naming
conventions for our fork. The RPM may not be required for any
currently supported test deployments and changes should be examined
prior to deployment.

.. note::

  Unless there are unexpected circumstances this will be the last full
  release of the HPC enhanced fork. Future releases will leverage
  GitLab's custom executor model with a substantially diminished
  number of source changes required.

Admin Changes
-------------

* Corrected error handling for call to
  `LookupGroupID <https://golang.org/pkg/os/user/#LookupGroupId>`_ that in
  extraordinarily specific circumstances could lead to a job crashing
  during preparation.
