GitLab CI Basics
================

The goal of this example is to introduce a simple two stage pipeline that
builds and tests a "Hello World" program. Doing so will introduce some concepts
of the GitLab runner with a simple
`shell executor <https://docs.gitlab.com/runner/executors/shell.html>`_
with the Setuid enhancements. For more details on any of the keywords
used here, additional documentation can be found in
`GitLab's yaml documentation <https://docs.gitlab.com/ee/ci/yaml/>`_.

.. tabs::

    .. tab:: Generic

        .. literalinclude:: ../../source/gitlab-ci-basics/.gitlab-ci.generic.yml
            :language: yaml
            :linenos:

    .. tab:: ALCF

        .. literalinclude:: ../../source/gitlab-ci-basics/.gitlab-ci.alcf.yml
            :language: yaml
            :linenos:

    .. tab:: OLCF

        .. literalinclude:: ../../source/gitlab-ci-basics/.gitlab-ci.olcf.yml
            :language: yaml
            :linenos:

    .. tab:: GitLab

        .. literalinclude:: ../../source/gitlab-ci-basics/.gitlab-ci.gitlab.yml
            :language: yaml
            :linenos:

Source
------

main.c
~~~~~~

.. literalinclude:: ../../source/gitlab-ci-basics/main.c
     :language: c
     :linenos:
