.. |br| raw:: html

    <br>

Federation Testing Documentation
================================

.. important::

    All federation enhancements are currently under active development.
    Future changes to this model can/will occur without notification.
    As such this documentation should be considered only useful for
    enabling collaborative testing of the related enhancements.

Jacamar - Federated
~~~~~~~~~~~~~~~~~~~

The goal of federation within the
`authorization flow <jacamar/auth.html#authorization-flow>`_
is fundamentally to translate a GitLab server user to their local account.
This is done by validating an authentication token provided by a managed IdP
to the GitLab server.

.. code-block:: toml

    [auth.runas]
      validation_script = "/opt/jacamar/scripts/federation.py"
      federated = true

Once enabled you will be expected to act upon the additional context
provided to you ``validation_script`` else the federated username
will be trusted without further validation. For additional details on the
defaults context see `RunAs User <jacamar/auth.html#runas-user>`_
documentation.

.. list-table::
    :widths: 10 50
    :header-rows: 1

    * - Key
      - Description
    * - ``FEDERATED_AUTH_TOKEN``
      - An authentication provider recognized tokens identified in the federation process. This is only present when Federated is enabled.
    * - ``FEDERATED_USERNAME``
      - An authentication provider identified username of the associated accounted. This is only present when Federated is enabled.

In addition to the new context, the username provided via command
line arguments (e.g. ``RUNAS_CURRENT_USER`` environment
variable) will properly convey any username provided via the
IdP as opposed to the GitLab login.

.. hidden-code-block:: python
    :starthidden: True
    :label: Example Python validation_script for Google Auth (federated enabled).

    #!/usr/bin/python3
    import requests
    import json
    import os
    import datetime

    ## User table (can be replaced with an LDAP lookup, for example)
    IDENTITIES = [
        {'extern_uid': '1234567890', 'provider': 'google', 'username': 'userA'}
    ]

    def lookup(gitlab_federated_username):
        for identity in IDENTITIES:
            if identity['extern_uid'] == gitlab_federated_username:
                return identity
        else:
            raise RuntimeError("External user {extern_uid} not found.".format(extern_uid=gitlab_federated_username))

    ## IdP backchannels for independent validation of the user context
    PROVIDERS = {
        'google':
        {
            'endpoint': 'https://www.googleapis.com/oauth2/v3/tokeninfo'
        }
    }

    def validate(gitlab_auth_token, provider):
        try:
            headers = {"Authorization": "Bearer {token}".format(token=gitlab_auth_token)}
            response = requests.get(PROVIDERS[provider]['endpoint'], headers=headers)
            response.raise_for_status()
        except Exception as e:
            raise RuntimeError("{exception}".format(exception=e))

    ## Remapping the external uid to the local username
    def remap(identity):
        return json.dumps({'username': identity['username']})

    ## Validation workflow
    def validate_auth(gitlab_federated_username, gitlab_auth_token):
        # Lookup external user in local user management system
        identity = lookup(gitlab_federated_username)

        # Validate user context against IdP
        validate(gitlab_auth_token, 'google')

        # Remap external user to local user
        return remap(identity)


    if __name__ == '__main__':

        # Grab user context from the runner
        gitlab_federated_username = str(os.getenv('FEDERATED_USERNAME'))
        gitlab_auth_token = str(os.getenv('FEDERATED_AUTH_TOKEN'))

        with open("/etc/gitlab-runner/validate_auth.log", 'a') as logfile:
            try:
                validated_user = validate_auth(gitlab_federated_username, gitlab_auth_token)
            except Exception as e:
                error_message = "FAILED {error}".format(error=str(e))
                logfile.write("{date} {error}\n".format(date=str(datetime.datetime.now()), error=error_message))
                raise RuntimeError(error_message)
            else:
                success_message = "PASSED {message}".format(message=validated_user)
                logfile.write("{date} {message}\n".format(date=str(datetime.datetime.now()), message=success_message))
                print(validated_user)

|br|
